package com.jppesola.cucumber.searchtest;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class SearchSteps {

    WebDriver driver;

    @Given("Open the {string} browser")
    public void open_browser(String browser) {
        browser = browser.toLowerCase();
        if (browser.equals("chrome")) {
            driver = new ChromeDriver();
        } else if (browser.equals("firefox")) {
            driver = new FirefoxDriver();
        } else {
            Assert.assertTrue("Unsupported browser " + browser, false);
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Given("Navigate to {string}")
    public void navigate_to_address(String url) {
        driver.navigate().to(url);
    }

    @When("Search for {string}")
    public void do_the_search(String searchString) {
        driver.findElement(By.xpath("/html//input[@id='twotabsearchtextbox']")).sendKeys(searchString);
        driver.findElement(By.xpath("//div[@id='nav-search']/form[@role='search']//input[@value='Go']")).click();
    }

    @Then("Sort the results by price")
    public void sort_by_price() {
        driver.findElement(By.cssSelector(".a-dropdown-label")).click();
        driver.findElement(By.linkText("Price: High to Low")).click();
    }

    @And("Select the result number {int}")
    public void select_result(int n) {
        driver.findElement(By.cssSelector("[data-component-id] [class='sg-col-20-of-24 s-result-item"
                + " sg-col-0-of-12 sg-col-28-of-32 sg-col-16-of-20"
                + " sg-col sg-col-32-of-36 sg-col-12-of-16 sg-col-24-of-28']:nth-of-type("
                + n
                + ") .a-size-medium")).click();
    }

    @Then("Verify that the result title contains {string}")
    public void verify_title(String subTitle) {
        Assert.assertTrue("String '" + subTitle + "' not found from the title: " + driver.getTitle(),
                driver.getTitle().contains(subTitle));
    }

    //Close the browser window always after test scenarios
    @After
    public void close_the_browser() {
        driver.close();
    }

}
