Feature: Search functionality and sorting of results

  Scenario: Search Amazon with Google Chrome and sort the results

    Given Open the "Chrome" browser
    Given Navigate to "https://www.amazon.com/"
    When Search for "Nikon"
    Then Sort the results by price
    And Select the result number 2
    Then Verify that the result title contains "Nikon D3X"

  Scenario: Search Amazon with Mozilla Firefox and sort the results

    Given Open the "Firefox" browser
    Given Navigate to "https://www.amazon.com/"
    When Search for "Nikon"
    Then Sort the results by price
    And Select the result number 2
    Then Verify that the result title contains "Nikon D3X"